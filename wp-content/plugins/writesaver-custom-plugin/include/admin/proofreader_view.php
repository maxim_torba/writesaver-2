<?php
/*
 *  Writesaver View
 */
wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');
wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
wp_enqueue_style('admin-responsive-tab-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive-tab.css', '', '', 'all');
wp_enqueue_style('admin-responsive-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/responsive.css', '', '', 'all');
wp_enqueue_style('admin-style_uv-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style_uv.css', '', '', 'all');
wp_enqueue_script('admin-custom-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');

global $wpdb;
$proof_id = $_GET['proof_id'];
$test_status = get_user_meta($proof_id, 'test_status', TRUE);
$test_completed = get_user_meta($proof_id, 'test_completed', TRUE);

$test_results = $wpdb->get_results("SELECT *  FROM tbl_proofreader_test WHERE fk_proofreader_id = $proof_id GROUP BY test_id ");
$test_count = count($test_results);

if ($test_count != 5 || $test_status == 'accepted' || $test_status == 'rejected' || $test_completed != TRUE) {
    print('<script>window.location.href="admin.php?page=proofreader_list"</script>');
    exit;
}
$proof_info = get_userdata($proof_id);
?>
<div class="load_overlay" id="loding">
    <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
</div>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1><?php echo $proof_info->first_name . ' ' . $proof_info->last_name; ?></h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="wrap_div">
            <label> Email: </label>
            <p><?php echo $proof_info->user_email; ?></p>
        </div>
    </div>
    <input type="hidden" id="proof_id" value="<?php echo $proof_id; ?>" />

    <div class="blog_category_sticky profile_setting">
        <div class="container">
            <div class="blog_category_sticky_left">
                <div class="desktop_catagory">
                    <div class="category_full_list">
                        <div class="category_btn">
                            <div class="category_btn_icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/cat_icon.png" class="img-responsive">
                            </div>
                            <div class="category_btn_txt">
                                <span>Tests</span>
                            </div>
                        </div>
                        <ul id="links">
                            <?php
                            foreach ($test_results as $test) {
                                $test_id = $test->test_id;
                                ?>
                                <li class="<?php echo ($test_id == 1) ? 'active' : ''; ?>" id="note_<?php echo $test_id; ?>"><?php echo get_the_title($test->post_id) ?></li>
                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog_category_main" id="notContent">
        <div class="container">
            <?php
            foreach ($test_results as $test) {
                $test_id = $test->test_id;
                ?>
                <div id="notContent_<?php echo $test_id; ?>" class="blog_listt">           
                    <div class="row">
                        <div class="col-sm-2">
                            <h4>Question: </h4>
                        </div>
                        <div class="col-sm-10">
                            <p><?php echo $test->test_question; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <h4>Answer: </h4>
                        </div>
                        <div class="col-sm-10">
                            <p><?php echo $test->test_submitted; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <h4>Timing Used: </h4>
                        </div>
                        <div class="col-sm-10">
                            <p><?php echo $test->timing_used; ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="test_status">
        <div class="container">
            <div class="wrap_div">
                <p>Status: </p>
                <select class="status" <?php echo ($test_status == 'rejected') ? 'style="pointer-events:none";' : ''; ?>>
                    <option value="">Select Status</option>
                    <option <?php echo ($test_status == 'accepted') ? 'selected' : ''; ?> value="accepted">Accepted</option>
                    <option  <?php echo ($test_status == 'delayed') ? 'selected' : ''; ?>  value="delayed">Delayed</option>
                    <option   <?php echo ($test_status == 'rejected') ? 'selected' : ''; ?>  value="rejected">Rejected</option>
                </select>
            </div>
        </div>
        <div class="status_msg"></div>
    </div>
</section>
<script>
    jQuery(document).ready(function () {
        jQuery('.status').change(function (event) {
            var button = jQuery(this);
            jQuery('.statusmsg').remove();
            var status = jQuery(this).val();
            var proof_id = jQuery('#proof_id').val();
            if (status) {
                var r = confirm("Are you sure to change status to " + status + "?");
                if (r == true) {
                    jQuery('#loding').show();
                    jQuery.ajax({
                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                        type: "POST",
                        data: {
                            action: 'change_proof_status',
                            proof_id: proof_id,
                            status: status
                        },
                        success: function (data) {
                            jQuery('#loding').hide();
                            if (data == 1)
                                jQuery(".status_msg").html('<span class="text-success statusmsg">Request ' + status + ' sucessfully... </span>');
                            else
                                jQuery(".status_msg").html('<span class="text-danger statusmsg">Request not ' + status + ' sucessfully...</span>');
                            jQuery(".statusmsg").fadeOut(5000);

                            if (status == 'rejected' || status == 'accepted') {
                                debugger;
                                button.css('pointer-events', 'none');
                                location.reload();
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            jQuery('#loding').hide();
                            console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        }
                    });
                } else {
                    $(r).dialog("close");
                }
            }
            return false;
        });
    });
</script>

