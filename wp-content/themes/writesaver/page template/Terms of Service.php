<?php
/*
 * Template Name: Terms of service
 */

get_header();
?>

<section>

    <div class="breadcum">

        <div class="container">

            <div class="page_title">

                <?php the_title('<h1>', '</h1>'); ?>

            </div>

        </div>

    </div>

</section>

<section>

    <div class="term_service">
        <div class="container">
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>