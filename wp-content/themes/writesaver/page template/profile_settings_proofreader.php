
<?php
/*
 * Template Name: profile_settings_proofreader
 */
global $wpdb;


get_header();
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}

$user_id = get_current_user_id();
$old_profile_pic = get_user_meta($user_id, 'profile_pic_url', true);
$dummy_image = get_template_directory_uri() . '/images/customer.png';
if ($old_profile_pic) {
    $profile_pic = $old_profile_pic;
    $display = 'block';
} else {
    $profile_pic = $dummy_image;
    $display = 'none';
}

$prefix = $wpdb->prefix;
$notification_table = $prefix . 'proofreader_notification_setting';
$current_user = wp_get_current_user();

$result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $notification_table WHERE user_id = %d ", $user_id));

$desktop_new_doc = "";
$desktop_other_notification = "";
$email_info_about_availability = "";
$email_relevant_stories = "";

if (count($result) > 0) {
    $desktop_new_doc = ($result[0]->desktop_new_doc == 1 ? "checked" : "");
    $desktop_other_notification = ($result[0]->desktop_other_notification == 1 ? "checked" : "");
    $email_info_about_availability = ($result[0]->email_info_about_availability == 1 ? "checked" : "");
    $email_relevant_stories = ($result[0]->email_relevant_stories == 1 ? "checked" : "");
}
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Settings</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="blog_category_sticky profile_setting">
        <div class="container">
            <div class="blog_category_sticky_left">
                <div class="desktop_catagory">
                    <div class="category_full_list">
                        <div class="category_btn">
                            <div class="category_btn_icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/cat_icon.png" class="img-responsive">
                            </div>
                            <div class="category_btn_txt">
                                <span>Settings</span>
                            </div>
                        </div>
                        <ul id="links">
                            <li id="note_1"  class="active">Profile settings</li>
                            <li id="note_2" >Tax settings</li>
                            <li id="note_3" >Get paid </li>
                            <li id="note_4" > Notification settings</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog_category_main" id="notContent">
        <div class="container">
            <div id="notContent_1" class="blog_listt">
                <div class="row">
                    <form id="proofreader_profile" name="proofreader_profile" method="POST" action="">
                        <div class="col-sm-2">
                            <?Php
                            if (is_user_logged_in()) {
                                $user_id = get_current_user_id();
                                global $wpdb;
                                $prefix = $wpdb->prefix;
                                $usermeta = $prefix . 'usermeta';
                                $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $usermeta WHERE user_id = %d ", $user_id));
                            }
                            ?>

                            <div class="main_profile_img_block">
                                <div class="main_profile_img">
                                    <img src="<?php echo $profile_pic; ?>" alt="" id="profile_pic" class="image-responsive output" />
                                    <input type="hidden" id="old_pic" value="<?php echo $profile_pic; ?>" />
                                    <input type="hidden" id="dummy_pic" value="<?php echo $dummy_image; ?>" />

                                </div>
                                <div class="main_profile_remove" style="display: none">
                                    <a href="#" class="red_link remove_old_img" style="display: <?php echo $display; ?>">Remove</a>
                                    <a href="#" class="red_link remove_img" style="display: none">Remove</a>

                                    <input type="hidden" name="remove_image" id="remove_image" value="" />

                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                        <span class="btn btn-default btn-file"><span>Upload Profile</span>
                                            <input type="hidden" value="" name="">                                                        
                                            <input type="file" name="profile_pic" id="profile_picture"  class="fileinput_info" onchange="allvalidateimageFile(this.value, 'profile_img')"/>
                                        </span>
                                        <span class="fileinput-filename">No file chosen</span>
                                        <span class="fileinput-new">No file chosen</span>
                                        <div class="pic_msg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="setting_right">

                                <div class="field_title">
                                    <h4>Personal info </h4>
                                    <div class="edit_link" >
                                        <a href="javascript:void(0);" class="edit_pro" id='edit_info'></a>      </div>
                                </div>
                                <div class="all_fields">
                                    <div class="row">
                                        <input type='hidden' id='user_id' name='user_id' value='<?php echo $user_id; ?>'>
                                        <div class="col-sm-6">
                                            <input type="text" data-fname="<?php echo $current_user->user_firstname; ?>" class="pro_info only_alpha" placeholder="First Name*" id="fname" name="fname" value="<?php echo $current_user->user_firstname; ?>" disabled="">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"  data-lname="<?php echo $current_user->user_lastname; ?>"  class="pro_info only_alpha"  id="lname" placeholder="Last Name*" name="lname" value="<?php echo $current_user->user_lastname; ?>" disabled="true">
                                        </div>

                                        <div class="col-sm-6">
                                            <input type="text"  data-add1="<?php echo get_usermeta($user_id, 'address1', TRUE) ?>"   class="pro_info" placeholder="Address Line1*"  id="add1" name="add1" value="<?php echo get_usermeta($user_id, 'address1', TRUE) ?>"  disabled="true">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"  data-add2="<?php echo get_usermeta($user_id, 'address2', TRUE) ?>"  class="pro_info" placeholder="Address Line2"  id="add2" name="add2" value="<?php echo get_usermeta($user_id, 'address2', TRUE) ?>"  disabled="true">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"   data-zip_code="<?php echo get_usermeta($user_id, 'zip_code', TRUE) ?>"  class="pro_info only_num" placeholder="Zip Code*" maxlength="6"  id="zip_code" name="zip_code" value="<?php echo get_usermeta($user_id, 'zip_code', TRUE) ?>"  disabled="true" >
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"   data-P_state="<?php echo get_usermeta($user_id, 'state', TRUE) ?>"  class="pro_info only_alpha" placeholder="State*"  id="P_state" name="P_state" value="<?php echo get_usermeta($user_id, 'state', TRUE) ?>"  disabled="true">
                                        </div>

                                    </div>
                                    <div class="profile_msg"></div>
                                </div>
                                <div class="field_title">
                                    <h4>Education & University</h4>
                                    <div class="edit_link">
                                        <a href="javascript:void(0);" class="edit_pro " id='edit_university'></a> 
                                    </div>
                                </div>
                                <div class="all_fields">
                                    <?php $total_edu = get_user_meta($user_id, 'total_edu', TRUE); ?>
                                    <div class="row edu_parent">
                                        <input type="hidden" id="edu_count" name="edu_count" value="<?php
                                        if ($total_edu > 0)
                                            echo $total_edu;
                                        else
                                            echo '1';
                                        ?>">
                                               <?php
                                               if ($total_edu > 0):
                                                   for ($i = 1; $i <= $total_edu; $i++) {
                                                       ?>
                                                <div class="edu_info" id="uni_<?php echo $i; ?>">
                                                    <div class="col-sm-6">
                                                        <input type="text" data-degree="<?php echo get_usermeta($user_id, 'degree' . $i, TRUE) ?>" placeholder="Degree of*" class="university_info edu_degree" id='degree<?php echo $i; ?>' name='degree<?php echo $i; ?>' value="<?php echo get_usermeta($user_id, 'degree' . $i, TRUE) ?>"  disabled="true">
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" data-uni="<?php echo get_usermeta($user_id, 'university' . $i, TRUE) ?>" placeholder="University*" class="university_info edu_uni" id='uni<?php echo $i; ?>' name='uni<?php echo $i; ?>' value="<?php echo get_usermeta($user_id, 'university' . $i, TRUE) ?>" disabled="true">
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <a  style="display: none;" class="del_edu old_edu" href="javascript:void(0);" data-edu="<?php echo $i; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            <?php } else: ?>
                                            <div class="edu_info" id="uni_1">
                                                <div class="col-sm-6">
                                                    <input type="text" data-degree="" placeholder="Degree of*" class="university_info edu_degree" id='degree1' name='degree1'   disabled="true">
                                                </div>
                                                <div class="col-sm-5">
                                                    <input type="text" data-uni="" placeholder="University*" class="university_info edu_uni" id='uni1' name='uni1'  disabled="true">
                                                </div>

                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <a href="javascript:void(0);" class="add_edu" style="display: none">Add new</a>
                                    <div class="university_msg"></div>
                                </div>
                                <div class="field_title">
                                    <h4>Country info</h4>
                                    <div class="edit_link">
                                        <a href="javascript:void(0);" class="edit_pro " id='edit_country'></a> 
                                    </div>
                                </div>
                                <div class="all_fields">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input data-country_citizenship="<?php echo get_usermeta($user_id, 'country_citizenship', TRUE) ?>" class="country_info only_alpha"  type="text" id='country_citizenship' name='country_citizenship' value="<?php echo get_usermeta($user_id, 'country_citizenship', TRUE) ?>" placeholder="Country of Citizenship*" disabled="true">
                                        </div>
                                        <div class="col-sm-6">
                                            <input data-country_cresidence="<?php echo get_usermeta($user_id, 'country_cresidence', TRUE) ?>" class="country_info only_alpha"  type="text" name='country_cresidence' id='country_cresidence' value="<?php echo get_usermeta($user_id, 'country_cresidence', TRUE) ?>" placeholder="Country of Residence*" disabled="true">
                                        </div>
                                    </div>
                                    <div class="country_msg"></div>
                                </div>
                                <div class="field_title">
                                    <h4>Job History</h4>
                                    <div class="edit_link">
                                        <a href="javascript:void(0);" class="edit_job edit_pro " id='edit_job'></a> 
                                    </div>
                                </div>
                                <div class="all_fields">
                                    <div class="row ">
                                        <?php
                                        $jobs = $wpdb->get_results("SELECT * FROM job_history WHERE proof_id = $user_id");
                                        $total_job = count($jobs);
                                        ?>
                                        <input type="hidden" id="job_count" name="job_count" value="<?php
                                        if ($total_job > 0)
                                            echo $total_job;
                                        else
                                            echo '1';
                                        ?>">
                                        <input type="hidden" value="" id="del_job" name="del_job" />
                                        <input type="hidden" value="" id="rem_resume" name="rem_resume" />
                                        <div class="job_table">
                                            <div class="job_parent">
                                                <?php
                                                if ($total_job > 0):
                                                    $count = 0;
                                                    foreach ($jobs as $job) {
                                                        $count++;
                                                        ?>
                                                        <div class="job_history" id="job_<?php echo $count; ?>">

                                                            <div class="col-sm-6">
                                                                <input  disabled="true" value="<?php echo $job->company_name; ?>"  type="text" data-cname="<?php echo $job->company_name; ?>" placeholder="company Name*" class="job_info job_cname contact_block" id='cname<?php echo $count; ?>' name='cname[]'   >
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input  disabled="true"  value="<?php echo $job->designation; ?>" type="text" data-designation="<?php echo $job->designation; ?>" placeholder="Designation*" class="job_info job_des contact_block" id='designation<?php echo $count; ?>' name='designation[]' >
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input  disabled="true" value="<?php echo date("d-m-Y", strtotime($job->start_date)); ?>"   type="text" data-start_date="<?php echo date("d-m-Y", strtotime($job->start_date)); ?>" placeholder="Start date*" class="job_date job_info job_sdate contact_block" id='sdate<?php echo $count; ?>' name='sdate[]'   >
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <input  disabled="true"  value="<?php echo date("d-m-Y", strtotime($job->end_date)); ?>"  type="text" data-end_date="<?php echo date("d-m-Y", strtotime($job->end_date)); ?>" placeholder="End date*" class="job_date job_info job_edate contact_block" id='edate<?php echo $count; ?>' name='edate[]'   >
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <a style="display: none;"  class="del_job old_job delete" href="javascript:void(0);" data-job="<?php echo $count; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                                <input type="hidden" class="job_id" name="job_id[]" value="<?php echo $job->job_id; ?>"/>
                                                            </div>
                                                        </div>
                                                    <?php } else: ?>
                                                    <div class="job_history" id="job_1">
                                                        <div class="col-sm-6">
                                                            <input type="text" data-cname="" placeholder="company Name*"  disabled="true" class="job_info job_cname contact_block" id='cname1' name='cname[]'   >
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" data-designation="" placeholder="Designation*"  disabled="true" class="job_info job_des contact_block" id='designation1' name='designation[]' >
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" data-start_date="" placeholder="Start date*"  disabled="true" class="job_date job_info job_sdate contact_block" id='sdate1' name='sdate[]'   >
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" data-end_date="" placeholder="End date*"  disabled="true" class="job_date job_info job_edate contact_block" id='edate1' name='edate[]'   >
                                                        </div>

                                                    </div>
                                                <?php endif; ?>                                               
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="fileinput fileinput-new fileinput-exists" data-provides="fileinput">
                                                    <span class="btn btn-default btn-file"><span>Upload Resume</span>
                                                        <input type="hidden" value="" name="">                                                        
                                                        <input type="file" disabled="" name="upload_resume" id="upload_resume"  class="fileinput_info job_info" onchange="validateresumeFile(this.value, 'upload_resume')"/>
                                                    </span>
                                                    <span class="fileinput-filename">No file chosen</span>
                                                    <span class="fileinput-new">No file chosen</span>
                                                    <div class="pic_msg"></div>
                                                </div>
                                                <?php
                                                if (get_user_meta($user_id, 'job_resume', true)):
                                                    echo '<div class="resume_div">';
                                                    $url = get_user_meta($user_id, 'job_resume', true);
                                                    echo '<a href="' . $url . '" target="_blank">' . $name = basename($url) . '</a>';
                                                    echo '<a href="javascript:void(0);" class="remove_resume" style="display:none;"><i class="fa fa-times" aria-hidden="true"></i> Remove</a>';
                                                    echo '</div>';
                                                endif;
                                                ?>                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <a style="display: none;"  href="javascript:void(0);" class="add_job add_new" >Add new</a>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="job_msg"></div>
                                        </div>
                                    </div>

                                </div>

                                <div class="field_title">
                                    <h4>Change email</h4>

                                </div>
                                <div class="all_fields">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" value="<?php echo $current_user->user_email; ?>" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="field_title">
                                    <h4>Change password</h4>
                                    <div class="edit_link" id='pass_popup'>
                                        <a href="#" role="button" data-toggle="modal" data-target="#pass_modal" class="chg_pass edit_pro"></a>
                                    </div>
                                </div>

                            </div>
                            <div class="main_profile_delete">
                                <a href="#" class="red_link" onclick="delete_acc(<?php echo $user_id; ?>)">Delete account..</a>
                                <div class="del_msg"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div id="notContent_2" class="blog_listt">

                <div class="col-sm-7">
                    <form id="tax_save" name="tax_save" method="POST">
                        <?php
                        $user_tax = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id = $user_id LIMIT 1");
                        ?>
                        <h4>W-9</h4>

                        <div class="collect">
                            <h6>To collect the right information, indicate if you are a <span>U.S. person</span>:</h6>
                            <div class="radio_option">
                                <input name="radio" type="radio"  <?php if ($user_tax[0]->Is_Us_Person == 0) echo 'checked'; ?> id="rd1" value="0">
                                <label for="rd1">I am not a U.S. person</label>
                                <input name="radio" type="radio" <?php if ($user_tax[0]->Is_Us_Person == 1 || $user_tax[0]->Is_Us_Person = '') echo 'checked'; ?> id="rd2" value="1">
                                <label for="rd2" id="tax_us">I am a U.S. person</label>
                            </div>
                            <p>Before withdrawing funds, all <span>U.S. person</span> must provide their w-9 tax information.</p>
                        </div>
                        <div class="collect">
                            <h6>Legal name of business</h6>
                            <input type="text" placeholder="Business Email*" class="contact_block" name="Business_Legal_Name" id="Business_Legal_Name" value="<?php echo $user_tax[0]->Business_Legal_Name; ?>" >
                            <p>Provide the same name as shown on your tax return</p>
                        </div>
                        <div class="collect address">
                            <h6>Address <a href="#" id="change_add">change</a>
                                <a href="#" id="cancel_add" style="display: none">Cancel</a>
                            </h6>
                            <p id="p_address">
                                <?php echo $user_tax[0]->Address; ?>
                            </p>
                            <textarea class="contact_block" id="changed_add" name="changed_add" style="display: none;"><?php echo $user_tax[0]->Address; ?> </textarea>
                        </div>
                        <div class="collect">
                            <h6>Federal tax classification</h6>
                            <select class="contact_block" id="ddl_f_tax" name="federal_tax_classification">
                                <option value="individual/sole proprietor or single-member LLC">individual/sole proprietor or single-member LLC</option>
                                <option value="individual/sole proprietor or single-member LLC1">individual/sole proprietor or single-member LLC1</option>
                                <option value="individual/sole proprietor or single-member LLC2">individual/sole proprietor or single-member LLC2</option>
                            </select>
                        </div>
                        <div class="collect">
                            <h6>Taxpayer identification number type</h6>
                            <div class="radio_option">
                                <input name="radio1" <?php if ($user_tax[0]->Is_SSN == 1 || $user_tax[0]->Is_SSN = '') echo 'checked'; ?> type="radio" id="rd3" value="1">
                                <label for="rd3">Social Security Number (SSN)</label>
                                <input name="radio1" type="radio" <?php if ($user_tax[0]->Is_SSN == 0) echo 'checked'; ?> id="rd4" value="0">
                                <label for="rd4" id="tax_id">Employer identification number (EIN)</label>
                            </div>
                            <p>
                                An employer identification number (EIN) is a U.S. tax identification number (TIN) issued to businesses.
                                For additional information, or to apply for an EIN, you may visit this website.
                            </p>
                        </div>
                        <div class="collect">
                            <h6>SSN/EIN #</h6>
                            <input type="password" placeholder="* * * * * * " class="contact_block" name="SSN_EIN" id="SSN_EIN" value="<?php echo $user_tax[0]->SSN_EIN; ?>">
                        </div>
                        <div class="collect">
                            <div class="remember">
                                <input id="remember" <?php if ($user_tax[0]->Certify_under_penalties_perjury == 1) echo 'checked'; ?> type="checkbox" name="remember"  value="1" ><label for="remember">Certify, under penalties of perjury, that:</label>
                                <ul>
                                    <li>
                                        The number shown on this form is my correct
                                        taxpayer identification number (or I am waiting for
                                        a number to be issued to me); and
                                    </li>
                                    <li>
                                        I am not subject to backup withholding because:
                                        (a) I am exempt from backup withholding, or (b) I
                                        have not been notified by the Internal Revenue
                                        Service (IRS) that i am subject to backup
                                        withholdings as a result of a failure to report all
                                        interest or dividends, or (c) the IRS has notified me
                                        that I am no longer subject to backup withholdings;
                                        and
                                    </li>
                                    <li>
                                        I am a U.S. citizen or other <span>U.S. person</span>
                                    </li>
                                </ul>
                                <p><span>Note:</span> Only check this certification if all of these items are true;otherwise <span>contact help center</span></p>
                            </div>                                
                        </div>
                        <div class="buttons">
                            <input type="button" id="save_tax" value="Save" class="btn_sky">
                            <input type="reset" value="Cancel" class="btn_sky">
                        </div>

                    </form>
                </div>
                <div class="tax_msg"></div>
            </div>

            <div id="notContent_3" class="blog_listt">
                <?php
                $user_tax = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id = $user_id LIMIT 1");
                ?>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="setting_right">
                            <div class="field_title">
                                <h4>Paypal ID </h4>
                                <div class="edit_link">
                                    <a href="javascript:void(0);" class="edit_pro" id='edit_paypal'></a>      </div>
                            </div>
                        </div>
                        <form id='change_Paypal' name='change_Paypal' method="post">
                            <div class="all_fields">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $user_id = get_current_user_id(); ?>
                                        <input type="email" class="paypal_info contact_block" name='paid_id' id='paid_id' value="<?php echo $user_tax[0]->Paypal_ID; ?>" placeholder="Email*" disabled="true">
                                    </div>

                                </div>
                                <div class="paypal_msg"></div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <div id="notContent_4" class="blog_listt">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="setting_right notification_settings">
                            <div class="field_title">
                                <h4>Receive a desktop notification </h4>
                            </div>
                            <div class="setting_checkbox">
                                <div class="s_checkbox">
                                    <input id="checkbox-1" <?php echo $desktop_new_doc; ?> class="checkbox-custom" name="desktop_new_doc" type="checkbox">
                                    <label for="checkbox-1" class="checkbox-custom-label">There are new documents to review </label>
                                </div>
                                <div class="s_checkbox">
                                    <input id="checkbox-2" <?php echo $desktop_other_notification; ?> class="checkbox-custom" name="desktop_other_notification" type="checkbox">
                                    <label for="checkbox-2" class="checkbox-custom-label">Other relevant notifications letting you know about the availability of documents to proofread </label>
                                </div>
                            </div>
                            <div class="field_title">
                                <h4>Receive an email</h4>
                            </div>
                            <div class="setting_checkbox last_field_checkbox">
                                <div class="s_checkbox">
                                    <input id="checkbox-3" <?php echo $email_info_about_availability; ?> class="checkbox-custom" name="email_info_about_availability" type="checkbox">
                                    <label for="checkbox-3" class="checkbox-custom-label">We have information about the availability of documents to proofread </label>
                                </div>
                                <div class="s_checkbox">
                                    <input id="checkbox-4" <?php echo $email_relevant_stories; ?> class="checkbox-custom" name="email_relevant_stories" type="checkbox">
                                    <label for="checkbox-4" class="checkbox-custom-label">We publish stories relevant to your interests</label>
                                </div>
                            </div>
                            <div class="noti_msg"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<div id="pass_modal" class="pop_overlay" style="display: none;">
    <div class="pop_main">
        <div class="pop_head">
            <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
        </div>
        <div class="pop_body">
            <div class="page_title">
                <h2>Change Password</h2>
            </div>
            <div class="pop_content">
                <form method="post" id="profile_pass" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="password" class="txt_box contact_block" name="pass" id="pass"  maxlength="30"  placeholder="Current Password*">
                        </div>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="password" class="txt_box contact_block" name="new_pass" id="new_pass"  maxlength="30"  placeholder="New Password*">
                        </div>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="password" class="txt_box contact_block" name="conf_pass" id="conf_pass"  maxlength="30" placeholder="Confirm New Password*">
                        </div>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" name="text" class="submit btn_sky" value="Change Password">                                    
                        </div>
                        <div class="content pass_msg"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>    

<script>
    function validateresumeFile(file, resume_name) {
        $('.pic_msg1').remove();
        var ext = file.split(".");
        ext = ext[ext.length - 1].toLowerCase();
        var arrayExtensions = ["pdf", "doc", "docx"];
        if (arrayExtensions.lastIndexOf(ext) == -1) {
            $(".pic_msg").html('<span class="text-danger job_err ">Wrong extension type.Please upload valid file</span>');
            $(".job_err").fadeOut(5000);
            $("#upload_resume").val('');
            return false;
        } else {
            $('#rem_resume').val("");
            return true;
        }
    }
//Compare dates 
    function compareDate(start_date, end_date) {
        var date = start_date.substring(0, 2);
        var month = start_date.substring(3, 5);
        var year = start_date.substring(6, 10);
        var start_date = new Date(year, month - 1, date);
        var currentDate = new Date();
        if (end_date != '') {
            var edate = end_date.substring(0, 2);
            var emonth = end_date.substring(3, 5);
            var eyear = end_date.substring(6, 10);
            var end_date = new Date(eyear, emonth - 1, edate);
            if (end_date > currentDate) {
                return false;
            } else if (end_date < start_date) {
                return false;
            } else {
                return true;
            }
        } else {
            if (start_date > currentDate) {
                return false;
            } else {
                return true;
            }
        }
    }
    function DeleteEdu(id) {
        var edu_count = $('#edu_count').val();
        edu_count--;
        $("#uni_" + id).remove();
        $('#edu_count').val(edu_count);
    }
    function delete_acc(user_id) {
        var txt;
        var r = confirm("Are you sure to delete your account?");
        if (r == true) {
            $.ajax({
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                type: "POST",
                data: {
                    action: 'delete_acc',
                    user_id: user_id,
                },
                success: function (data) {
                    $('#loding').hide();
                    if (data == 1)
                        $(".del_msg").html('<span class="text-success delmsg">Account deleted sucessfully... </span>');
                    else
                        $(".del_msg").html('<span class="text-danger delmsg">Not deleted sucessfully...</span>');
                    $(".delmsg").fadeOut(5000);
                    window.setTimeout(function () {
                        location.reload();
                    }, 1000);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#loding').hide();
                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
        } else {
            $(r).dialog("close");
        }
        return false;
    }
    //Email Validation  
    function validateEmail(email) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email)) {
            return true;
        } else {
            return false;
        }
    }
    //---------------Check Image type---------//
    function allvalidateimageFile(file, imageName) {

        var ext = file.split(".");
        ext = ext[ext.length - 1].toLowerCase();
        var arrayExtensions = ["jpg", "jpeg", "png"];
        if (arrayExtensions.lastIndexOf(ext) == -1) {

            $('#profile_pic').attr('src', $('#old_pic').val());
            $(".pic_msg").html('<span class="text-danger pic_msg1 ">Wrong extension type.Please upload valid file</span>');
            $(".pic_msg1").fadeOut(5000);
            $("#profile_picture").val('');
            return false;
        } else if ($('#profile_picture')[0].files[0].size > 5242880) {

            $('#profile_pic').attr('src', $('#old_pic').val());
            $(".pic_msg").html('<span class="text-danger pic_msg1 ">Please upload max 5MB size file.</span>');
            $(".pic_msg1").fadeOut(5000);
            $("#profile_picture").val('');
            return false;
        } else {
            $('.remove_img').show();
            $('.remove_old_img').hide();
            $('#remove_image').val(0);
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('profile_pic');
                output.src = reader.result;
            };
            reader.readAsDataURL($('#profile_picture')[0].files[0]);
            return true;
        }
    }

    $('.checkbox-custom').click(function () {
        $('.notify_msg').remove();
        $('#loding').show();
        var fieldname1 = $(this).attr('name');
        var fieldvalue1 = "false";
        if ($(this).is(":checked")) {
            fieldvalue1 = "true";
        } else {
            fieldvalue1 = "false";
        }

        $.ajax({
            url: "<?php echo admin_url('admin-ajax.php'); ?>",
            type: "POST",
            data: {
                action: "fnupdatenotificationsettings",
                fieldvalue: fieldvalue1,
                fieldname: fieldname1,
            },
            dataType: "html",
            success: function (data) {
                $(".noti_msg").html('<span class="text-success notify_msg ">Notification settings saved successfully..</span>');
                $('#loding').hide();
                $(".notify_msg").fadeOut(3000);
            },
            error: function (err) {
                $(".noti_msg").html('<span class="text-danger notify_msg ">Notification settings not saved successfully...</span>');
                $('#loding').hide();
                $(".notify_msg").fadeOut(3000);
            }
        });
    });
    jQuery(document).ready(function ($) {

        jQuery('.job_date').datepicker({
            format: 'dd-mm-yyyy'
        });
        $(".remove_resume").live('click', function (e) {

            $('.resume_div').hide();
            $('#rem_resume').val(1);
        });
        $(".new_job").live('click', function (e) {

            var id = $(this).attr("data-history");
            var job_count = $('#job_count').val();
            job_count--;
            $("#job_" + id).remove();
            $('#job_count').val(job_count);
            var total_flag = 0;
            $(".job_history:visible").each(function (i) {

                var id = this.id;
                if ($('#' + id + ':visible')) {
                    total_flag = total_flag + 1;
                    $(this).attr('id', 'job_' + total_flag);
                    $('#job_' + total_flag + ' a.del_job').attr('data-history', total_flag);
                }

            });
            $('#job_count').val(total_flag);
        });
        $('.add_job').click(function () {
            event.preventDefault();
            var job_count = parseInt($('#job_count').val()) + 1;
            $('#job_count').val(job_count);
            $('.job_parent').append('<div class="job_history" id="job_' + job_count + '">' +
                    '<div class="col-sm-6">' +
                    '<input type="text" data-cname="" placeholder="company Name*" class="job_info job_cname contact_block" id="cname' + job_count + '" name="cname[]" >' +
                    '</div><div class="col-sm-6">' +
                    '<input class="job_info job_des contact_block" data-designation placeholder="Designation*" type="text" id="designation' + job_count + '" name="designation[]" >' +
                    '</div>' +
                    '<div class="col-sm-6">' +
                    '<input type="text" data-start_date="" placeholder="Start date*" class="job_date job_info job_sdate contact_block" id="sdate' + job_count + '" name="sdate[]" >' +
                    '</div><div class="col-sm-5">' +
                    '<input type="text" data-end_date="" placeholder="End date*" class="job_date job_info job_edate contact_block" id="edate' + job_count + '" name="edate[]" >' +
                    '</div>' +
                    '<div class="col-sm-1"><a data-history="' + job_count + '" class="del_job new_job delete"  href="javascript:void(0);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div>');
            jQuery('.job_date').datepicker({format: 'dd-mm-yyyy'});
            return false;
        });
        $(".old_job").live('click', function (e) {
            var del_job = $('#del_job').val();
            var job_id = $(this).next('.job_id').val();
            var id = $(this).attr("data-job");
            if (del_job)
                var deleted_ids = del_job + ',' + job_id;
            else
                var deleted_ids = job_id;
            $("#job_" + id).hide();
            $('#del_job').val(deleted_ids);
        });
        $("#edit_job").live('click', function (e) {

            $('.del_job').css('display', 'block');
            $('.add_job').css('display', 'block');
            $('.remove_resume').css('display', 'block');
            $('.job_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_job" class="save_pro"></a><a href="javascript:void(0);" id="cancel_job" class="cancel_pro"></a>');
            $(this).hide();
        });
        $("#save_job").live('click', function (e) {
            var button = $(this);
            $('.job_err').remove();
            var statusflag = true;
            $('.job_cname:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Company name is required.</span>');
                }
            });
            $('.job_des:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Designation is required.</span>');
                }
            });
            $('.job_sdate:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Start date is required.</span>');
                } else {

                    var start_date = $(this).val();
                    var sdate_flag = compareDate(start_date, '');
                    if (sdate_flag == false) {
                        statusflag = false;
                        $(this).after('<span class="text-danger job_err">Enter valid start date.</span>');
                    }
                }
            });
            $('.job_edate:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">End date is required.</span>');
                } else {
                    var end_date = $(this).val();
                    var start_date = $(this).parent().prev().find('.job_sdate').val();
                    var edate_flag = compareDate(start_date, end_date);
                    if (edate_flag == false) {
                        statusflag = false;
                        $(this).after('<span class="text-danger job_err">Enter valid end date.</span>');
                    }
                }
            });
            if (statusflag == true) {
                var form = $("#proofreader_profile")[0];
                var formdata = new FormData(form);
                formdata.append('action', 'save_jobhistory');
                $('#loding').show();
                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: formdata,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data == 1) {
                            $(".job_msg").html('<span class="text-success job_err">Updated successfully...</span>');
                            $.ajax({
                                type: "POST",
                                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                data: {
                                    action: 'get_job'
                                },
                                success: function (data) {
                                    $('.job_table').html(data);

                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                                }
                            });
                        } else {
                            $(".job_msg").html('<span class="text-danger job_err">Not updated successfully...</span>');
                        }
                        $(".job_err").fadeOut(5000);
                        $('.job_info').prop("disabled", true);
                        $('#edit_job').show();
                        $('#cancel_job').remove();
                        $('#save_job').remove();
                        $('.add_job').css('display', 'none');
                        $('.del_job').removeClass('new_job');
                        $('.del_job').addClass('old_job');
                        $('.del_job').css('display', 'none');
                        setTimeout(function () {
                            $("#upload_resume").val('');
                            $('#del_job').val('');
                            $('#rem_resume').val('');
                            var total_flag = 0;
                            $(".job_history:visible").each(function (i) {
                                var id = this.id;
                                if ($('#' + id + ':visible')) {
                                    total_flag = total_flag + 1;
                                    $(this).attr('id', 'job_' + total_flag);
                                    $('#job_' + total_flag + ' a.del_job').attr('data-job', total_flag);
                                }
                                $('#job_count').val(total_flag);
                            });
                            $('#loding').hide();
                        }, 2000);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
        });
        $("#cancel_job").live('click', function (e) {
            $('#loding').show();
            $('.job_err').remove();
            $(".job_history").each(function (i) {
                var id = this.id;
                if ($('#' + id + '  .new_job').length) {
                    $('#' + id).remove();
                }
                if ($('#' + id + '  .old_job').length) {
                    $('#' + id).show();
                }

            });
            $('.resume_div').show();
            $("#upload_resume").val('');
            $('#del_job').val('');
            $('#rem_resume').val('');

            $('.remove_resume').css('display', 'none');
            var total_flag = 0;
            $(".job_history:visible").each(function (i) {
                var id = this.id;
                if ($('#' + id + ':visible')) {
                    total_flag = total_flag + 1;
                    $(this).attr('id', 'job_' + total_flag);
                    $('#job_' + total_flag + ' a.del_job').attr('data-job', total_flag);
                }
                $('#job_count').val(total_flag);
            });
            $('.job_cname').each(function () {
                $(this).val($(this).attr("data-cname"));
            });
            $('.job_des').each(function () {
                $(this).val($(this).attr("data-designation"));
            });
            $('.job_edate').each(function () {
                $(this).val($(this).attr("data-end_date"));
            });
            $('.job_sdate').each(function () {
                $(this).val($(this).attr("data-start_date"));
            });
            $('.new_job').remove();
            $('.del_job').css('display', 'none');
            $('.add_job').css('display', 'none');
            $('.job_info').prop("disabled", true);
            $('#edit_job').show();
            $('#save_job').remove();
            $('.job_err').remove();
            $(this).remove();
            $('#loding').hide();
        });
        $('.add_edu').click(function () {
            event.preventDefault();
            var counter = parseInt($('#edu_count').val()) + 1;
            $('#edu_count').val(counter);
            $('.edu_parent').append('<div class="edu_info" id="uni_' + counter + '"><div class="col-sm-6">' +
                    '<input type="text" data-degree="" placeholder="Degree of*" class="university_info edu_degree" id="degree' + counter + '" name="degree' + counter + '" >' +
                    '</div><div class="col-sm-5">' +
                    '<input class="university_info edu_uni" data-uni="" placeholder="University*" type="text" id="uni' + counter + '" name="uni' + counter + '" >' +
                    '</div>' +
                    '<div class="col-sm-1"><a data-edu="' + counter + '" class="del_edu new_edu"  href="javascript:void(0);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div>');
            return false;
        });
        $('.address  #change_add').live('click', function (e) {
            e.preventDefault();
            $('#p_address').hide();
            $('#cancel_add').show();
            $('#changed_add').show();
            $(this).hide();
        });
        $('.address  #cancel_add').live('click', function (e) {
            e.preventDefault();
            $('#p_address').show();
            $('#change_add').show();
            $('#changed_add').hide();
            $(this).hide();
        });
        $('#save_tax').on('click', function () {

            $('.tax_err').remove();
            var Business_Legal_Name = $('#Business_Legal_Name').val();
            var SSN_EIN = $('#SSN_EIN').val();
            var error = false;
            if (!$('input[name=radio]:radio').val())
            {
                $('#tax_us').after('<span class="text-danger tax_err">Check if you are a U.S. person or not.</span>');
                error = true;
            }
            if (!$('input[name=radio1]:radio').val())
            {
                $('#tax_id').after('<span class="text-danger tax_err">Check taxpayer identification number type</span>');
                error = true;
            }
            if (Business_Legal_Name == '')
            {
                $('#Business_Legal_Name').after('<span class="text-danger tax_err">Legal name of business is required</span>');
                error = true;
            }
            if (SSN_EIN == '')
            {
                $('#SSN_EIN').after('<span class="text-danger tax_err">SSN/EIN is required</span>');
                error = true;
            }
            if (!$("#remember").is(":checked")) {
                $('#remember').next('label').after('<span class="text-danger tax_err">Please certify, under penalties of perjury</span>');
                error = true;
            }
            if (error == false) {
                $('#loding').show();
                //here tax setting
                var Is_Us_Person = $('input[name=radio]:radio').val();
                var Is_SSN = $('input[name=radio1]:radio').val();
                var Certify_under_penalties_perjury = $("#remember").is(":checked");
                var federal_tax_classification = $('#ddl_f_tax').val();
                if (!$('#changed_add').is(":hidden")) {
                    var address = $('#changed_add').val();
                }


                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: {
                        action: 'save_tax',
                        Is_Us_Person: Is_Us_Person,
                        Business_Legal_Name: Business_Legal_Name,
                        address: address,
                        federal_tax_classification: federal_tax_classification,
                        Is_SSN: Is_SSN,
                        SSN_EIN: SSN_EIN,
                        Certify_under_penalties_perjury: federal_tax_classification
                    },
                    dataType: "html",
                    success: function (data) {
                        $('#loding').hide();
                        if (data == 0)
                            $(".tax_msg").html('<span class="text-danger tax_err">Not updated successfully...</span>');
                        else
                            $(".tax_msg").html('<span class="text-success tax_err">Updated successfully...</span>');
                        $(".tax_err").fadeOut(5000);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
            return false;
        });
        $("#edit_country").live('click', function (e) {
            $('.save_pro').remove();
            $('.cancel_pro').remove();
            $('.country_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_country" class="save_pro"></a><a href="javascript:void(0);" id="cancel_country" class="cancel_pro"></a>');
            $(this).hide();
        });
        $("#cancel_country").live('click', function (e) {
            $('#loding').show();
            $('#country_citizenship').val($('#country_citizenship').attr("data-country_citizenship"));
            $('#country_cresidence').val($('#country_cresidence').attr("data-country_cresidence"));
            $('.country_err').remove();
            $('.country_info').prop("disabled", true);
            $('#edit_country').show();
            $('#save_country').remove();
            $(this).remove();
            $('#loding').hide();
        });
        $("#save_country").live('click', function (e) {

            $('.country_err').remove();
            var country_citizenship = $('#country_citizenship').val();
            var country_cresidence = $('#country_cresidence').val();
            var error = false;
            if (country_citizenship == '')
            {
                $('#country_citizenship').after('<span class="text-danger country_err">Country of citizenship is required</span>');
                error = true;
            }
            if (country_cresidence == '')
            {
                $('#country_cresidence').after('<span class="text-danger country_err">Country of residence is required.</span>');
                error = true;
            }

            if (error == false) {
                $('#loding').show();
                $.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    type: "POST",
                    data: {
                        action: 'save_country',
                        country_citizenship: country_citizenship,
                        country_cresidence: country_cresidence
                    },
                    success: function (data) {
                        $('#loding').hide();
                        if (data == 1) {
                            $('#country_citizenship').attr("data-country_citizenship", country_citizenship);
                            $('#country_cresidence').attr("data-country_cresidence", country_cresidence);
                            $(".country_msg").html('<span class="text-success countrymsg">Updated successfully...</span>');
                        } else
                            $(".country_msg").html('<span class="text-danger countrymsg">Not updated successfully...</span>');
                        $(".countrymsg").fadeOut(5000);
                        $('.country_info').prop("disabled", true);
                        $('#edit_country').show();
                        $('#cancel_country').remove();
                        $('#save_country').remove();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
        });
        $("#edit_paypal").live('click', function (e) {
            $('.save_pro').remove();
            $('.cancel_pro').remove();
            $('.paypal_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_paypal" class="save_pro"></a><a href="javascript:void(0);" id="cancel_paypal" class="cancel_pro"></a>');
            $(this).hide();
        });
        $("#cancel_paypal").live('click', function (e) {
            $('.paypal_err').remove();
            $('.paypal_info').prop("disabled", true);
            $('#edit_paypal').show();
            $('#save_paypal').remove();
            $(this).remove();
        });
        $("#save_paypal").live('click', function (e) {
            $('.paypal_err').remove();
            var paid_id = $('#paid_id').val();
            if (paid_id == '') {
                $('#paid_id').after('<span class="text-danger paypal_err">Paypal id is required.</span>');
                return false;
            } else if (!validateEmail(paid_id)) {
                jQuery('#paid_id').after('<span class="text-danger paypal_err">Please enter valid paypal id.</span>');
                return false;
            } else {
                $('#loding').show();
                $.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    type: "POST",
                    data: {
                        action: 'save_paypal',
                        paid_id: paid_id
                    },
                    success: function (data) {
                        $('#loding').hide();
                        if (data == 0)
                            $(".paypal_msg").html('<span class="text-danger paypalmsg">Not updated successfully...</span>');
                        else
                            $(".paypal_msg").html('<span class="text-success paypalmsg">Updated successfully...</span>');
                        $('.paypal_info').prop("disabled", true);
                        $('#edit_paypal').show();
                        $('#cancel_paypal').remove();
                        $('#save_paypal').remove();
                        $(".paypalmsg").fadeOut(5000);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
        });
        $("#edit_university").live('click', function (e) {

            $('.del_edu').css('display', 'block');
            $('.add_edu').css('display', 'block');
            $('.university_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_university" class="save_pro"></a><a href="javascript:void(0);" id="cancel_university" class="cancel_pro"></a>');
            $(this).hide();
        });
        $(".new_edu").live('click', function (e) {
            var id = $(this).attr("data-edu");
            var edu_count = $('#edu_count').val();
            edu_count--;
            $("#uni_" + id).remove();
            $('#edu_count').val(edu_count);
            var total_flag = 0;
            $(".edu_info").each(function (i) {
                var id = this.id;
                if ($('#' + id + ':visible')) {

                    total_flag = total_flag + 1;
                    $(this).attr('id', 'uni_' + total_flag);
                    $('#uni_' + total_flag + ' a.del_edu').attr('data-edu', total_flag);
                }
                $('#edu_count').val(total_flag);
            });
        });
        $(".old_edu").live('click', function (e) {

            var id = $(this).attr("data-edu");
            $("#uni_" + id).hide();
        });
        $(".new_edu").live('click', function (e) {

            var id = $(this).attr("data-edu");
            $("#uni_" + id).remove();
            var total_flag = 0;
            $(".edu_info:visible").each(function (i) {
                var id = this.id;
                if ($('#' + id + ':visible')) {

                    total_flag = total_flag + 1;
                    $(this).attr('id', 'uni_' + total_flag);
                    $('#uni_' + total_flag + ' a.del_edu').attr('data-edu', total_flag);
                }

            });
            $('#edu_count').val(total_flag);
        });
        $("#cancel_university").live('click', function (e) {

            $(".edu_info").each(function (i) {
                var id = this.id;
                if ($('#' + id + '  .new_edu').length) {
                    $('#' + id).remove();
                }
                if ($('#' + id + '  .old_edu').length) {
                    $('#' + id).show();
                }

            });
            var total_flag = 0;
            $(".edu_info:visible").each(function (i) {

                var id = this.id;
                if ($('#' + id + ':visible')) {
                    total_flag = total_flag + 1;
                    $(this).attr('id', 'uni_' + total_flag);
                    $('#uni_' + total_flag + ' a.del_edu').attr('data-edu', total_flag);
                }
                $('#edu_count').val(total_flag);
            });
            $('.edu_degree').each(function () {
                $(this).val($(this).attr("data-degree"));
            });
            $('.edu_uni').each(function () {
                $(this).val($(this).attr("data-uni"));
            });
            $('.new_edu').remove();
            $('.del_edu').css('display', 'none');
            $('.add_edu').css('display', 'none');
            $('.university_info').prop("disabled", true);
            $('#edit_university').show();
            $('#save_university').remove();
            $('.university_err').remove();
            $(this).remove();
        });
        $("#save_university").live('click', function (e) {
            $('.university_err').remove();
            var statusflag = true;
            $('.edu_degree:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger university_err">Degree is required.</span>');
                }
            });
            $('.edu_uni:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger university_err">University is required.</span>');
                }
            });
            if (statusflag == true) {

                var education = [];
                $(".edu_info:visible").each(function (i) {
                    var id = this.id;
                    var edu_degree = $('#' + id + '  .edu_degree').val();
                    var edu_uni = $('#' + id + ' .edu_uni').val();
                    education[i] = '{"edu_degree":"' + edu_degree + '","edu_uni":"' + edu_uni + '"}';
                });
                $('#loding').show();
                $.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    type: "POST",
                    data: {
                        action: 'save_university',
                        education: education
                    },
                    success: function (data) {


                        if (data == 1) {
                            var flag = 0;
                            education.forEach(function (e, index) {
                                var edu_array = JSON.parse(education[index]);
                                flag = flag + 1;
                                $('#degree' + flag).attr("data-degree", edu_array.edu_degree);
                                $('#uni' + flag).attr("data-uni", edu_array.edu_uni);
                            });
                            $(".edu_info").each(function (i) {
                                var id = this.id;
                                if ($('#' + id).css('display') == 'none')
                                {

                                    if ($('#' + id + '  .old_edu').length) {
                                        $('#' + id).remove();
                                    }
                                }

                            });
                            var total_flag = 0;
                            $(".edu_info").each(function (i) {
                                var id = this.id;
                                if ($('#' + id + ':visible')) {
                                    total_flag = total_flag + 1;
                                    $(this).attr('id', 'uni_' + total_flag);
                                    $('#uni_' + total_flag + ' a.del_edu').attr('data-edu', total_flag);
                                }
                                $('#edu_count').val(total_flag);
                            });
                            $(".university_msg").html('<span class="text-success universitymsg">Updated successfully...</span>');
                        } else
                            $(".university_msg").html('<span class="text-danger universitymsg">Not updated successfully...</span>');
                        $(".universitymsg").fadeOut(5000);
                        $('.university_info').prop("disabled", true);
                        $('#edit_university').show();
                        $('#cancel_university').remove();
                        $('#save_university').remove();
                        $('.add_edu').css('display', 'none');
                        $('.del_edu').removeClass('new_edu');
                        $('.del_edu').addClass('old_edu');
                        $('.del_edu').css('display', 'none');
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
        });
        $('.remove_img').click(function () {
            $('#profile_pic').attr('src', $('#old_pic').val());
            $("#profile_picture").val('');
            $(this).hide();
            $('#remove_image').val(1);
        });
        $('.remove_old_img').click(function () {
            $('#profile_pic').attr('src', $('#dummy_pic').val());
            $('#old_pic').val($('#dummy_pic').val());
            $("#profile_picture").val('');
            $('#remove_image').val(1);
            $(this).hide();
        });
        $('#edit_info').click(function () {
            $('.pro_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_info" class="save_pro"></a><a href="javascript:void(0);" id="cancel_info" class="cancel_pro"></a>');
            $(this).hide();
            $('.remove_image').show();
            $('.main_profile_remove').show();
        });
        $("#pass_popup").on('click', function () {
            $('#pass_modal').show();
        });
        $("#save_info").live('click', function (e) {

            $('.main_profile_remove').hide();
            $('.pro_err').remove();
            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var add1 = $('#add1').val();
            var add2 = $('#add2').val();
            var zip_code = $('#zip_code').val();
            var P_state = $('#P_state').val();
            var error = false;
            if (fname == '')
            {
                $('#fname').after('<span class="text-danger pro_err">First name is required.</span>');
                error = true;
            }
            if (lname == '')
            {
                $('#lname').after('<span class="text-danger pro_err">Last name is required.</span>');
                error = true;
            }
            if (add1 == '')
            {
                $('#add1').after('<span class="text-danger pro_err">Address is required.</span>');
                error = true;
            }

            if (zip_code == '')
            {
                $('#zip_code').after('<span class="text-danger pro_err">Zip code is required.</span>');
                error = 1;
            }
            if (P_state == '')
            {
                $('#P_state').after('<span class="text-danger pro_err">State is required.</span>');
                error = true;
            }
            if (error == false) {
                $('#loding').show();
                var alldata = $('#proofreader_profile').serialize();
                var old_pic = $('#old_pic').val();
                var form = $("#proofreader_profile")[0];
                var formdata = new FormData(form);
                formdata.append('alldata', alldata);
                formdata.append('action', 'save_basic');
                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: formdata,
                    dataType: "json",
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        if (data['image'] == '') {
                            $('.remove_img').hide();
                            $('.remove_old_img').hide();
                            $('#profile_pic').attr('src', old_pic);
                            $('.user_info img').attr('src', old_pic);
                        } else {
                            $('#profile_pic').attr('src', data['image']);
                            $('#old_pic').val(data['image']);
                            $('.remove_img').hide();
                            $('.remove_old_img').show();
                            $('.user img').attr('src', data['image']);
                        }
                        $('#fname').attr("data-fname", fname);
                        $('#lname').attr("data-lname", lname);
                        $('#add1').attr("data-add1", add1);
                        $('#add2').attr("data-add2", add2);
                        $('#zip_code').attr("data-zip_code", zip_code);
                        $('#P_state').attr("data-p_state", P_state);
                        $('.user_fname').text(fname);
                        $(".profile_msg").html(data['message']);
                        $(".pic_msg1").fadeOut(5000);
                        $('#edit_info').show();
                        $('.pro_info').prop("disabled", true);
                        $('#save_info').remove();
                        $('#cancel_info').remove();
                        $(".pro_err").fadeOut(5000);
                        $("#profile_picture").val('');
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        $('#loding').hide();
                    }
                });
            }
            return false;
        });
        $("#cancel_info").live('click', function (e) {
            $('#loding').show();
            $('#fname').val($('#fname').attr("data-fname"));
            $('#lname').val($('#lname').attr("data-lname"));
            $('#add1').val($('#add1').attr("data-add1"));
            $('#add2').val($('#add2').attr("data-add2"));
            $('#zip_code').val($('#zip_code').attr("data-zip_code"));
            $('#P_state').val($('#P_state').attr("data-p_state"));
            $('#profile_pic').attr('src', $('#old_pic').val());
            $('.main_profile_remove').hide();
            $('.pro_err').remove();
            $('#edit_info').show();
            $('.pro_info').prop("disabled", true);
            $('#save_info').remove();
            $('#cancel_info').remove();
            $("#profile_picture").val('');
            $('#loding').hide();
        });
    });

</script>
<?php get_footer(); ?>