<?php
/*
 * Template Name: Basic Information Review
 */

get_header();

$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}
$user_id = get_current_user_id();
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Basic information</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="blog_category_sticky basic_information">
        <div class="container">
            <div class="blog_category_sticky_left">
                <div class="desktop_catagory">
                    <div class=" close_tab">
                        <div class="category_btn">
                            <div class="category_btn_icon">
                                <img src="<?php echo get_template_directory_uri() ?>/images/cat_icon.png" class="img-responsive">
                            </div>
                            <div class="category_btn_txt">
                                <span>Informations</span>
                            </div>
                        </div>

                        <ul id="links">
                            <li class="active info_tab"><a href="javascript:void(0);"> Basic information  </a></li>
                            <li ><a href="javascript:void(0);" >  Proofreading ability </a></li>                                                               
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="blog_category_main" id="notContent">
        <div id="notContent_1" class="blog_listt">
            <div class="basic_info" id="vertical">
                <div class="container">
                    <div class="row basic_info_inner final_info">
                        <div class="col-sm-offset-1 col-sm-10">
                            <div class="field_title">
                                <h4>Personal info </h4>
                                <div class="edit_link">
                                    <a href="javascript:void(0);" class="edit_pro" id='edit_info'></a>
                                </div>
                            </div>
                            <div class="row">
                                <form method="post" id="proofreader_profile">
                                    <div class="all_fields">

                                        <div class="col-sm-6">
                                            <input type="text" data-fname="<?php echo $current_user->user_firstname; ?>" class="pro_info only_alpha" placeholder="First Name*" id="fname" name="fname" value="<?php echo $current_user->user_firstname; ?>" disabled="">
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"  data-lname="<?php echo $current_user->user_lastname; ?>"  class="pro_info only_alpha"  id="lname" placeholder="Last Name*" name="lname" value="<?php echo $current_user->user_lastname; ?>" disabled="true">
                                        </div>

                                        <div class="col-sm-6">
                                            <input type="text"  data-add1="<?php echo get_usermeta($user_id, 'address1', TRUE) ?>"   class="pro_info" placeholder="Address Line1*"  id="add1" name="add1" value="<?php echo get_usermeta($user_id, 'address1', TRUE) ?>"  disabled="true">
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"  data-add2="<?php echo get_usermeta($user_id, 'address2', TRUE) ?>"  class="pro_info" placeholder="Address Line2"  id="add2" name="add2" value="<?php echo get_usermeta($user_id, 'address2', TRUE) ?>"  disabled="true">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text"   data-zip_code="<?php echo get_usermeta($user_id, 'zip_code', TRUE) ?>"  class="pro_info only_num" placeholder="Zip Code*" maxlength="6"  id="zip_code" name="zip_code" value="<?php echo get_usermeta($user_id, 'zip_code', TRUE) ?>"  disabled="true" >
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text"   data-P_state="<?php echo get_usermeta($user_id, 'state', TRUE) ?>"  class="pro_info only_alpha" placeholder="State*"  id="P_state" name="P_state" value="<?php echo get_usermeta($user_id, 'state', TRUE) ?>"  disabled="true">
                                        </div>
                                    </div>
                                    <div class="profile_msg"></div>
                                </form>
                                <div class="col-sm-12">
                                    <div class="field_title">
                                        <h4>Education & University</h4>
                                        <div class="edit_link">
                                            <a href="javascript:void(0);" class="edit_pro " id='edit_university'></a> 
                                        </div>
                                    </div>
                                </div>
                                <form method="post" id="university_detail">
                                    <div class="all_fields">
                                        <?php $total_edu = get_user_meta($user_id, 'total_edu', TRUE); ?>
                                        <div class="row edu_parent">
                                            <input type="hidden" id="edu_count" name="edu_count" value="<?php
                                            if ($total_edu > 0)
                                                echo $total_edu;
                                            else
                                                echo '1';
                                            ?>">
                                                   <?php
                                                   if ($total_edu > 0):
                                                       for ($i = 1; $i <= $total_edu; $i++) {
                                                           ?>
                                                    <div class="edu_info" id="uni_<?php echo $i; ?>">
                                                        <div class="col-sm-6">
                                                            <input type="text" data-degree="<?php echo get_usermeta($user_id, 'degree' . $i, TRUE) ?>" placeholder="Degree of*" class="university_info edu_degree" id='degree<?php echo $i; ?>' name='degree<?php echo $i; ?>' value="<?php echo get_usermeta($user_id, 'degree' . $i, TRUE) ?>"  disabled="true">
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <input type="text" data-uni="<?php echo get_usermeta($user_id, 'university' . $i, TRUE) ?>" placeholder="University*" class="university_info edu_uni" id='uni<?php echo $i; ?>' name='uni<?php echo $i; ?>' value="<?php echo get_usermeta($user_id, 'university' . $i, TRUE) ?>" disabled="true">
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <a  style="display: none;" class="del_edu old_edu" href="javascript:void(0);" data-edu="<?php echo $i; ?>"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                <?php } else: ?>
                                                <div class="edu_info" id="uni_1">
                                                    <div class="col-sm-6">
                                                        <input type="text" data-degree="" placeholder="Degree of*" class="university_info edu_degree" id='degree1' name='degree1'   disabled="true">
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" data-uni="" placeholder="University*" class="university_info edu_uni" id='uni1' name='uni1'  disabled="true">
                                                    </div>

                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <a href="javascript:void(0);" class="add_edu" style="display: none">Add new</a>

                                    </div>
                                    <div class="university_msg"></div>
                                </form>
                                <div class="col-sm-12">
                                    <div class="field_title">
                                        <h4>Country info</h4>
                                        <div class="edit_link">
                                            <a href="javascript:void(0);" class="edit_pro " id='edit_country'></a> 
                                        </div>
                                    </div>
                                </div>
                                <form method="post" id="country_detail">
                                    <div class="all_fields">
                                        <div class="col-sm-6">
                                            <input data-country_citizenship="<?php echo get_usermeta($user_id, 'country_citizenship', TRUE) ?>" class="country_info only_alpha"  type="text" id='country_citizenship' name='country_citizenship' value="<?php echo get_usermeta($user_id, 'country_citizenship', TRUE) ?>" placeholder="Country of Citizenship*" disabled="true">
                                        </div>
                                        <div class="col-sm-5">
                                            <input data-country_cresidence="<?php echo get_usermeta($user_id, 'country_cresidence', TRUE) ?>" class="country_info only_alpha"  type="text" name='country_cresidence' id='country_cresidence' value="<?php echo get_usermeta($user_id, 'country_cresidence', TRUE) ?>" placeholder="Country of Residence*" disabled="true">
                                        </div>
                                    </div>
                                    <div class="country_msg" ></div>
                                </form>
                                <div class="col-sm-12">
                                    <div class="field_title">
                                        <h4>Job History</h4>
                                        <div class="edit_link">
                                            <a href="javascript:void(0);" class="edit_job edit_pro " id='edit_job'></a> 
                                        </div>
                                    </div>
                                </div>
                                <form method="post" id="job_detail">
                                    <div class="all_fields">
                                        <?php
                                        $jobs = $wpdb->get_results("SELECT * FROM job_history WHERE proof_id = $user_id");
                                        $total_job = count($jobs);
                                        ?>
                                        <div class="row ">
                                            <input type="hidden" id="job_count" name="job_count" value="<?php
                                            if ($total_job > 0)
                                                echo $total_job;
                                            else
                                                echo '1';
                                            ?>">
                                            <input type="hidden" value="" id="del_job" name="del_job" />
                                            <input type="hidden" value="" id="rem_resume" name="rem_resume" />
                                            <div class="job_table">
                                                <div class="job_parent">
                                                    <?php
                                                    if ($total_job > 0):
                                                        $count = 0;
                                                        foreach ($jobs as $job) {
                                                            $count++;
                                                            ?>
                                                            <div class="job_history" id="job_<?php echo $count; ?>">

                                                                <div class="col-sm-6">
                                                                    <input  disabled="true" value="<?php echo $job->company_name; ?>"  type="text" data-cname="<?php echo $job->company_name; ?>" placeholder="Company Name*" class="job_info job_cname contact_block" id='cname<?php echo $count; ?>' name='cname[]'   >
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <input  disabled="true"  value="<?php echo $job->designation; ?>" type="text" data-designation="<?php echo $job->designation; ?>" placeholder="Designation*" class="job_info job_des contact_block" id='designation<?php echo $count; ?>' name='designation[]' >
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input  disabled="true" value="<?php echo ($job->start_date)? date("d-m-Y", strtotime($job->start_date)) : '' ; ?>"   type="text" data-start_date="<?php echo date("d-m-Y", strtotime($job->start_date)); ?>" placeholder="Start Date*" class="job_date job_info job_sdate contact_block" id='sdate<?php echo $count; ?>' name='sdate[]'   >
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <input  disabled="true"  value="<?php echo ($job->end_date) ? date("d-m-Y", strtotime($job->end_date)) : ''; ?>"  type="text" data-end_date="<?php echo date("d-m-Y", strtotime($job->end_date)); ?>" placeholder="End Date*" class="job_date job_info job_edate contact_block" id='edate<?php echo $count; ?>' name='edate[]'   >
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <a style="display: none;"  class="del_job old_job delete" href="javascript:void(0);" data-job="<?php echo $count; ?>"><i class="fa fa-trash-o"></i></a>
                                                                    <input type="hidden" class="job_id" name="job_id[]" value="<?php echo $job->job_id; ?>"/>
                                                                </div>
                                                            </div>
                                                        <?php } else: ?>
                                                        <div class="job_history" id="job_1">
                                                            <div class="col-sm-6">
                                                                <input type="text" data-cname="" placeholder="company Name*"  disabled="true" class="job_info job_cname contact_block" id='cname1' name='cname[]'   >
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <input type="text" data-designation="" placeholder="Designation*"  disabled="true" class="job_info job_des contact_block" id='designation1' name='designation[]' >
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" data-start_date="" placeholder="Start date*"  disabled="true" class="job_date job_info job_sdate contact_block" id='sdate1' name='sdate[]'   >
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <input type="text" data-end_date="" placeholder="End date*"  disabled="true" class="job_date job_info job_edate contact_block" id='edate1' name='edate[]'   >
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <a style="display:none;"  class="del_job old_job delete" href="javascript:void(0);" data-job="1"><i class="fa fa-trash-o"></i></a>
                                                                <input type="hidden" class="job_id" name="job_id[]" value=""/>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>                                               
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                                        <span class="btn btn-default btn-file"><span>Upload Resume</span>
                                                            <input type="hidden" value="" name="">                                                        
                                                            <input type="file" disabled="" name="upload_resume" id="upload_resume"  class="fileinput_info job_info" onchange="validateresumeFile(this.value, 'upload_resume')"/>
                                                        </span>
                                                        <span class="fileinput-filename">No file chosen</span>
                                                        <span class="fileinput-new">No file chosen</span>
                                                        <div class="pic_msg"></div>
                                                    </div>
                                                    <?php
                                                    if (get_user_meta($user_id, 'job_resume', true)):
                                                        echo '<div class="resume_div">';
                                                        $url = get_user_meta($user_id, 'job_resume', true);
                                                        echo '<a href="' . $url . '" target="_blank">' . $name = basename($url) . '</a>';
                                                        echo '<a href="javascript:void(0);" class="remove_resume" style="display:none;"><i class="fa fa-times"></i>Remove</a>';
                                                        echo '</div>';
                                                    endif;
                                                    ?>

                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <a style="display: none;"  href="javascript:void(0);" class="add_job" >Add new</a>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="job_msg"></div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </form>
                                <div class="col-sm-12">
                                    <input id="final_review" type="submit" value="Continue to next step" class="btn_sky">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="notContent_2" class="blog_listt">
            <div class="proofreading_info">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">    
                            <div class="proofreading_info_step">
                                <?php
                                $post = get_post(772);
                                echo $content = remove_vc_from_excerpt($post->post_content);
                                ?>
                                <div class="btn_blue">
                                    <a href="<?php echo get_the_permalink(774); ?>" class="btn_sky">Start</a>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function validateresumeFile(file, resume_name) {
        debugger;
        $('.pic_msg1').remove();
        var ext = file.split(".");
        ext = ext[ext.length - 1].toLowerCase();
        var arrayExtensions = ["pdf", "doc", "docx"];
        if (arrayExtensions.lastIndexOf(ext) == -1) {
            $(".pic_msg").html('<span class="text-danger job_err ">Wrong extension type.Please upload valid file</span>');
            $(".job_err").fadeOut(5000);
            $("#upload_resume").val('');
            return false;
        } else {
            $('#rem_resume').val("");
            return true;
        }
    }
//Compare dates 
    function compareDate(start_date, end_date) {
        var date = start_date.substring(0, 2);
        var month = start_date.substring(3, 5);
        var year = start_date.substring(6, 10);
        var start_date = new Date(year, month - 1, date);
        var currentDate = new Date();
        if (end_date != '') {
            var edate = end_date.substring(0, 2);
            var emonth = end_date.substring(3, 5);
            var eyear = end_date.substring(6, 10);
            var end_date = new Date(eyear, emonth - 1, edate);
            if (end_date > currentDate) {
                return false;
            } else if (end_date < start_date) {
                return false;
            } else {
                return true;
            }
        } else {
            if (start_date > currentDate) {
                return false;
            } else {
                return true;
            }
        }
    }

    jQuery(document).ready(function ($) {
        /***datepicker***/
        jQuery('.job_date').datepicker({
            format: 'dd-mm-yyyy'
        });

        $(".remove_resume").live('click', function (e) {

            $('.resume_div').hide();
            $('#rem_resume').val(1);
        });


        $(".new_job").live('click', function (e) {

            var id = $(this).attr("data-history");
            var job_count = $('#job_count').val();
            job_count--;
            $("#job_" + id).remove();
            $('#job_count').val(job_count);
            var total_flag = 0;
            $(".job_history:visible").each(function (i) {

                var id = this.id;
                if ($('#' + id + ':visible')) {
                    total_flag = total_flag + 1;
                    $(this).attr('id', 'job_' + total_flag);
                    $('#job_' + total_flag + ' a.del_job').attr('data-history', total_flag);
                }

            });
            $('#job_count').val(total_flag);
        });
        $('.add_job').click(function () {
            event.preventDefault();
            var job_count = parseInt($('#job_count').val()) + 1;
            $('#job_count').val(job_count);
            $('.job_parent').append('<div class="job_history" id="job_' + job_count + '">' +
                    '<div class="col-sm-6">' +
                    '<input type="text" data-cname="" placeholder="Company Name*" class="job_info job_cname contact_block" id="cname' + job_count + '" name="cname[]" >' +
                    '</div><div class="col-sm-5">' +
                    '<input class="job_info job_des contact_block" data-designation placeholder="Designation*" type="text" id="designation' + job_count + '" name="designation[]" >' +
                    '</div>' +
                    '<div class="col-sm-6">' +
                    '<input type="text" data-start_date="" placeholder="Start Date*" class="job_date job_info job_sdate contact_block" id="sdate' + job_count + '" name="sdate[]" >' +
                    '</div><div class="col-sm-5">' +
                    '<input type="text" data-end_date="" placeholder="End Date*" class="job_date job_info job_edate contact_block" id="edate' + job_count + '" name="edate[]" >' +
                    '</div>' +
                    '<div class="col-sm-1"><a data-history="' + job_count + '" class="del_job new_job delete"  href="javascript:void(0);"><i class="fa fa-trash-o"></i></a></div></div>');
            jQuery('.job_date').datepicker({format: 'dd-mm-yyyy'});
            return false;
        });

        $(".old_job").live('click', function (e) {
            var del_job = $('#del_job').val();
            var job_id = $(this).next('.job_id').val();
            var id = $(this).attr("data-job");
            if (del_job)
                var deleted_ids = del_job + ',' + job_id;
            else
                var deleted_ids = job_id;
            $("#job_" + id).hide();
            $('#del_job').val(deleted_ids);
        });

        $('.close_tab #links  li').on('click', function () {
            if ($(this).hasClass("info_tab")) {
                $('.blog_category_sticky_left #links li:eq(' + eval($(this).index()) + ')').addClass('active').next().removeClass('active');
                $('.blog_category_main .blog_listt:eq(' + eval($(this).index()) + ')').show().next().hide()
            }
            return false;
        });

        $('#final_review').live('click', function (e) {
            $('.text-danger').remove();
            $('.review_err').remove();
            var button = $(this);
            var error = false;
            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var add1 = $('#add1').val();
            var zip_code = $('#zip_code').val();
            var P_state = $('#P_state').val();

            var country_citizenship = $('#country_citizenship').val();
            var country_cresidence = $('#country_cresidence').val();

            if (fname == '')
            {
                $('#fname').after('<span class="text-danger review_err">First name is required.</span>');
                error = true;
            }
            if (lname == '')
            {
                $('#lname').after('<span class="text-danger review_err">Last name is required.</span>');
                error = true;
            }
            if (add1 == '')
            {
                $('#add1').after('<span class="text-danger review_err">Address is required.</span>');
                error = true;
            }

            if (zip_code == '')
            {
                $('#zip_code').after('<span class="text-danger review_err">Zip code is required.</span>');
                error = 1;
            }
            if (P_state == '')
            {
                $('#P_state').after('<span class="text-danger review_err">State is required.</span>');
                error = true;
            }

            $('.edu_degree:visible').each(function () {
                if ($(this).val() == "") {
                    error = true;
                    $(this).after('<span class="text-danger review_err">Degree is required.</span>');
                }
            });
            $('.edu_uni:visible').each(function () {
                if ($(this).val() == "") {
                    error = true;
                    $(this).after('<span class="text-danger review_err">University is required.</span>');
                }
            });



            $('.job_cname:visible').each(function () {
                if ($(this).val() == "") {
                    error = true;
                    $(this).after('<span class="text-danger review_err">Company name is required.</span>');
                }
            });
            $('.job_sdate:visible').each(function () {
                if ($(this).val() == "") {
                    error = true;
                    $(this).after('<span class="text-danger review_err">Start date is required.</span>');
                } else {

                    var start_date = $(this).val();
                    var sdate_flag = compareDate(start_date, '');
                    if (sdate_flag == false) {
                        statusflag = false;
                        $(this).after('<span class="text-danger review_err">Enter valid start date.</span>');
                    }
                }
            });
            $('.job_edate:visible').each(function () {
                if ($(this).val() == "") {
                    error = true;
                    $(this).after('<span class="text-danger review_err">End date is required.</span>');
                } else {
                    var end_date = $(this).val();
                    var start_date = $(this).parent().prev().find('.job_sdate').val();
                    var edate_flag = compareDate(start_date, end_date);
                    if (edate_flag == false) {
                        statusflag = false;
                        $(this).after('<span class="text-danger review_err">Enter valid end date.</span>');
                    }
                }
            });
            $('.job_des:visible').each(function () {
                if ($(this).val() == "") {
                    error = true;
                    $(this).after('<span class="text-danger review_err">Designation is required.</span>');
                }
            });

            if (country_citizenship == '')
            {
                $('#country_citizenship').after('<span class="text-danger review_err">Country of citizenship is required.</span>');
                error = true;
            }
            if (country_cresidence == '')
            {
                $('#country_cresidence').after('<span class="text-danger review_err">Country of residence is required.</span>');
                error = true;
            }
            if (error == false) {
                $('.blog_category_sticky_left #links li:eq(' + eval(button.closest('div.blog_listt').index()) + ')').removeClass('active').next().addClass('active');
                $('.blog_category_main .blog_listt:eq(' + eval(button.closest('div.blog_listt').index()) + ')').hide().next().show();
                $('html,body').animate({
                    scrollTop: $('.basic_information').offset().top - 200},
                        '1000');
            }
            return false;
        });

        $('#edit_info').click(function () {
            $('.pro_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_info" class="save_pro"></a><a href="javascript:void(0);" id="cancel_info" class="cancel_pro"></a>');
            $(this).hide();
            $('.remove_image').show();
        });
        $("#save_info").live('click', function (e) {

            $('.pro_err').remove();
            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var add1 = $('#add1').val();
            var add2 = $('#add2').val();
            var zip_code = $('#zip_code').val();
            var P_state = $('#P_state').val();
            var error = false;
            if (fname == '')
            {
                $('#fname').after('<span class="text-danger pro_err">First name is required.</span>');
                error = true;
            }
            if (lname == '')
            {
                $('#lname').after('<span class="text-danger pro_err">Last name is required.</span>');
                error = true;
            }
            if (add1 == '')
            {
                $('#add1').after('<span class="text-danger pro_err">Address is required.</span>');
                error = true;
            }

            if (zip_code == '')
            {
                $('#zip_code').after('<span class="text-danger pro_err">Zip code is required.</span>');
                error = 1;
            }
            if (P_state == '')
            {
                $('#P_state').after('<span class="text-danger pro_err">State is required.</span>');
                error = true;
            }
            if (error == false) {

                $('#loding').show();

                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: $('#proofreader_profile').serialize() + "&action=save_basic",
                    dataType: "json",

                    success: function (data) {

                        $('#fname').attr("data-fname", fname);
                        $('#lname').attr("data-lname", lname);
                        $('#add1').attr("data-add1", add1);
                        $('#add2').attr("data-add2", add2);
                        $('#zip_code').attr("data-zip_code", zip_code);
                        $('#P_state').attr("data-p_state", P_state);

                        $('.user_fname').text(fname);
                        $(".profile_msg").html(data['message']);
                        $(".pic_msg1").fadeOut(5000);
                        $('#edit_info').show();
                        $('.pro_info').prop("disabled", true);
                        $('#save_info').remove();
                        $('#cancel_info').remove();
                        $(".pro_err").fadeOut(5000);
                        $('#loding').hide();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        $('#loding').hide();
                    }
                });
            }
            return false;
        });
        $("#cancel_info").live('click', function (e) {
            $('#loding').show();
            $('#fname').val($('#fname').attr("data-fname"));
            $('#lname').val($('#lname').attr("data-lname"));
            $('#add1').val($('#add1').attr("data-add1"));
            $('#add2').val($('#add2').attr("data-add2"));
            $('#zip_code').val($('#zip_code').attr("data-zip_code"));
            $('#P_state').val($('#P_state').attr("data-p_state"));

            $('.pro_err').remove();
            $('#edit_info').show();
            $('.pro_info').prop("disabled", true);
            $('#save_info').remove();
            $('#cancel_info').remove();
            $('.pro_err').remove();
            $('#loding').hide();
        });

        $(".new_edu").live('click', function (e) {

            var id = $(this).attr("data-edu");

            $("#uni_" + id).remove();
            var total_flag = 0;
            $(".edu_info:visible").each(function (i) {
                var id = this.id;
                if ($('#' + id + ':visible')) {

                    total_flag = total_flag + 1;
                    $(this).attr('id', 'uni_' + total_flag);
                    $('#uni_' + total_flag + ' a.del_edu').attr('data-edu', total_flag);
                }

            });
            $('#edu_count').val(total_flag);
        });

        $(".old_edu").live('click', function (e) {

            var id = $(this).attr("data-edu");
            $("#uni_" + id).hide();


        });

        $('.add_edu').click(function () {
            event.preventDefault();
            var counter = parseInt($('#edu_count').val()) + 1;
            $('#edu_count').val(counter);

            $('.edu_parent').append('<div class="edu_info" id="uni_' + counter + '"><div class="col-sm-6">' +
                    '<input type="text" data-degree="" placeholder="Degree of*" class="university_info edu_degree" id="degree' + counter + '" name="degree' + counter + '" >' +
                    '</div><div class="col-sm-5">' +
                    '<input class="university_info edu_uni" data-uni="" placeholder="University*" type="text" id="uni' + counter + '" name="uni' + counter + '" >' +
                    '</div>' +
                    '<div class="col-sm-1"><a data-edu="' + counter + '" class="del_edu new_edu"  href="javascript:void(0);"><i class="fa fa-trash-o"></i></a></div></div>');

            return false;
        });


        $("#edit_job").live('click', function (e) {

            $('.del_job').css('display', 'block');
            $('.add_job').css('display', 'block');
            $('.remove_resume').css('display', 'block');

            $('.job_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_job" class="save_pro"></a><a href="javascript:void(0);" id="cancel_job" class="cancel_pro"></a>');
            $(this).hide();
        });

        $("#save_job").live('click', function (e) {
            var button = $(this);
            $('.job_err').remove();
            var statusflag = true;
            $('.job_cname:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Company name is required.</span>');
                }
            });
            $('.job_des:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Designation is required.</span>');
                }
            });
            $('.job_sdate:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">Start date is required.</span>');
                } else {

                    var start_date = $(this).val();
                    var sdate_flag = compareDate(start_date, '');
                    if (sdate_flag == false) {
                        statusflag = false;
                        $(this).after('<span class="text-danger job_err">Enter valid start date.</span>');
                    }
                }
            });
            $('.job_edate:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger job_err">End date is required.</span>');
                } else {
                    var end_date = $(this).val();
                    var start_date = $(this).parent().prev().find('.job_sdate').val();
                    var edate_flag = compareDate(start_date, end_date);
                    if (edate_flag == false) {
                        statusflag = false;
                        $(this).after('<span class="text-danger job_err">Enter valid end date.</span>');
                    }
                }
            });
            if (statusflag == true) {
                var form = $("#job_detail")[0];
                var formdata = new FormData(form);
                formdata.append('action', 'save_jobhistory');
                $('#loding').show();
                $.ajax({
                    type: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    data: formdata,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data == 1) {
                            $(".job_msg").html('<span class="text-success job_err">Updated successfully...</span>');
                            $.ajax({
                                type: "POST",
                                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                data: {
                                    action: 'get_job'
                                },
                                success: function (data) {
                                    $('.job_table').html(data);
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                                }
                            });
                        } else {
                            $(".job_msg").html('<span class="text-danger job_err">Not updated successfully...</span>');
                        }
                        $(".job_err").fadeOut(5000);
                        $('.job_info').prop("disabled", true);
                        $('#edit_job').show();
                        $('#cancel_job').remove();
                        $('#save_job').remove();
                        $('.add_job').css('display', 'none');
                        $('.del_job').css('display', 'none');
                        setTimeout(function () {
                            $("#upload_resume").val('');
                            $('#del_job').val('');
                            $('#rem_resume').val('');
                            var total_flag = 0;
                            $(".job_history:visible").each(function (i) {
                                var id = this.id;
                                if ($('#' + id + ':visible')) {
                                    total_flag = total_flag + 1;
                                    $(this).attr('id', 'job_' + total_flag);
                                    $('#job_' + total_flag + ' a.del_job').attr('data-job', total_flag);
                                }
                                $('#job_count').val(total_flag);
                            });
                            $('#loding').hide();
                        }, 2000);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
        });

        $("#cancel_job").live('click', function (e) {
            $('#loding').show();
            $('.job_err').remove();
            $(".job_history").each(function (i) {
                var id = this.id;
                if ($('#' + id + '  .new_job').length) {
                    $('#' + id).remove();
                }
                if ($('#' + id + '  .old_job').length) {
                    $('#' + id).show();
                }

            });
            $('.resume_div').show();
            $("#upload_resume").val('');
            $('#del_job').val('');
            $('#rem_resume').val('');
            $('.remove_resume').css('display', 'none');
            var total_flag = 0;
            $(".job_history:visible").each(function (i) {
                var id = this.id;
                if ($('#' + id + ':visible')) {
                    total_flag = total_flag + 1;
                    $(this).attr('id', 'job_' + total_flag);
                    $('#job_' + total_flag + ' a.del_job').attr('data-job', total_flag);
                }
                $('#job_count').val(total_flag);

            });
            $('.job_cname').each(function () {
                $(this).val($(this).attr("data-cname"));
            });
            $('.job_des').each(function () {
                $(this).val($(this).attr("data-designation"));
            });
            $('.job_edate').each(function () {
                $(this).val($(this).attr("data-end_date"));
            });
            $('.job_sdate').each(function () {
                $(this).val($(this).attr("data-start_date"));
            });
            $('.new_job').remove();
            $('.del_job').css('display', 'none');
            $('.add_job').css('display', 'none');
            $('.job_info').prop("disabled", true);
            $('#edit_job').show();
            $('#save_job').remove();
            $('.job_err').remove();
            $(this).remove();
            $('#loding').hide();
        });

        $("#edit_university").live('click', function (e) {

            $('.del_edu').css('display', 'block');
            $('.add_edu').css('display', 'block');

            $('.university_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_university" class="save_pro"></a><a href="javascript:void(0);" id="cancel_university" class="cancel_pro"></a>');
            $(this).hide();
        });
        $("#cancel_university").live('click', function (e) {
            $('#loding').show();
            $(".edu_info").each(function (i) {
                var id = this.id;
                if ($('#' + id + '  .new_edu').length) {
                    $('#' + id).remove();
                }
                if ($('#' + id + '  .old_edu').length) {
                    $('#' + id).show();
                }

            });
            var total_flag = 0;
            $(".edu_info:visible").each(function (i) {

                var id = this.id;
                if ($('#' + id + ':visible')) {
                    total_flag = total_flag + 1;
                    $(this).attr('id', 'uni_' + total_flag);
                    $('#uni_' + total_flag + ' a.del_edu').attr('data-edu', total_flag);
                }
                $('#edu_count').val(total_flag);

            });

            $('.edu_degree').each(function () {
                $(this).val($(this).attr("data-degree"));

            });
            $('.edu_uni').each(function () {
                $(this).val($(this).attr("data-uni"));
            });

            $('.new_edu').remove();
            $('.del_edu').css('display', 'none');
            $('.add_edu').css('display', 'none');
            $('.university_info').prop("disabled", true);
            $('#edit_university').show();
            $('#save_university').remove();
            $('.university_err').remove();
            $(this).remove();
            $('#loding').hide();
        });
        $("#save_university").live('click', function (e) {
            $('.university_err').remove();
            var statusflag = true;
            $('.edu_degree:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger university_err">Degree is required.</span>');
                }
            });
            $('.edu_uni:visible').each(function () {
                if ($(this).val() == "") {
                    statusflag = false;
                    $(this).after('<span class="text-danger university_err">University is required.</span>');
                }
            });
            if (statusflag == true) {

                var education = [];
                $(".edu_info:visible").each(function (i) {
                    var id = this.id;
                    var edu_degree = $('#' + id + '  .edu_degree').val();
                    var edu_uni = $('#' + id + ' .edu_uni').val();

                    education[i] = '{"edu_degree":"' + edu_degree + '","edu_uni":"' + edu_uni + '"}';
                });

                $('#loding').show();
                $.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    type: "POST",
                    data: {
                        action: 'save_university',
                        education: education
                    },
                    success: function (data) {


                        if (data == 1) {
                            var flag = 0;
                            education.forEach(function (e, index) {
                                var edu_array = JSON.parse(education[index]);
                                flag = flag + 1;
                                $('#degree' + flag).attr("data-degree", edu_array.edu_degree);
                                $('#uni' + flag).attr("data-uni", edu_array.edu_uni);

                            });
                            $(".edu_info").each(function (i) {
                                var id = this.id;
                                if ($('#' + id).css('display') == 'none')
                                {

                                    if ($('#' + id + '  .old_edu').length) {
                                        $('#' + id).remove();
                                    }
                                }

                            });
                            var total_flag = 0;
                            $(".edu_info").each(function (i) {
                                var id = this.id;

                                if ($('#' + id + ':visible')) {
                                    total_flag = total_flag + 1;
                                    $(this).attr('id', 'uni_' + total_flag);
                                    $('#uni_' + total_flag + ' a.del_edu').attr('data-edu', total_flag);
                                }
                                $('#edu_count').val(total_flag);

                            });
                            $(".university_msg").html('<span class="text-success universitymsg">Updated successfully...</span>');
                        } else
                            $(".university_msg").html('<span class="text-danger universitymsg">Not updated successfully...</span>');
                        $(".universitymsg").fadeOut(5000);
                        $('.university_info').prop("disabled", true);
                        $('#edit_university').show();
                        $('#cancel_university').remove();
                        $('#save_university').remove();
                        $('.add_edu').css('display', 'none');
                        $('.del_edu').removeClass('new_edu');
                        $('.del_edu').addClass('old_edu');
                        $('.del_edu').css('display', 'none');
                        $('#loding').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
        });

        $("#edit_country").live('click', function (e) {
            $('.save_pro').remove();
            $('.cancel_pro').remove();
            $('.country_info').prop("disabled", false);
            $(this).after('<a href="javascript:void(0);" id="save_country" class="save_pro"></a><a href="javascript:void(0);" id="cancel_country" class="cancel_pro"></a>');
            $(this).hide();
        });
        $("#cancel_country").live('click', function (e) {
            $('#loding').show();
            $('#country_citizenship').val($('#country_citizenship').attr("data-country_citizenship"));
            $('#country_cresidence').val($('#country_cresidence').attr("data-country_cresidence"));
            $('.country_err').remove();
            $('.country_info').prop("disabled", true);
            $('#edit_country').show();
            $('#save_country').remove();
            $(this).remove();
            $('#loding').hide();
        });
        $("#save_country").live('click', function (e) {

            $('.country_err').remove();
            var country_citizenship = $('#country_citizenship').val();
            var country_cresidence = $('#country_cresidence').val();
            var error = false;
            if (country_citizenship == '')
            {
                $('#country_citizenship').after('<span class="text-danger country_err">Country of citizenship is required.</span>');
                error = true;
            }
            if (country_cresidence == '')
            {
                $('#country_cresidence').after('<span class="text-danger country_err">Country of residence is required.</span>');
                error = true;
            }

            if (error == false) {
                $('#loding').show();
                $.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    type: "POST",
                    data: {
                        action: 'save_country',
                        country_citizenship: country_citizenship,
                        country_cresidence: country_cresidence
                    },
                    success: function (data) {
                        $('#loding').hide();
                        if (data == 1) {
                            $('#country_citizenship').attr("data-country_citizenship", country_citizenship);
                            $('#country_cresidence').attr("data-country_cresidence", country_cresidence);
                            $(".country_msg").html('<span class="text-success countrymsg">Updated successfully...</span>');
                        } else
                            $(".country_msg").html('<span class="text-danger countrymsg">Not updated successfully...</span>');
                        $(".countrymsg").fadeOut(5000);
                        $('.country_info').prop("disabled", true);
                        $('#edit_country').show();
                        $('#cancel_country').remove();
                        $('#save_country').remove();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#loding').hide();
                        console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    }
                });
            }
        });
    });
</script>
<?php get_footer(); ?>