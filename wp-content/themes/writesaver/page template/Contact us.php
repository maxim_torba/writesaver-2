<?php

/* 
 * Template Name: Contact us
 */

get_header();
?>

  <section>

            <div class="breadcum">

                <div class="container">

                    <div class="page_title">

                        <?php the_title( '<h1>', '</h1>' ); ?>

                    </div>

                </div>

            </div>

        </section>
   <section>

            <div class="contact_main">

                <div class="container">

                    <div class="contact_form_title">

                        <h4>Get in touch with us.</h4>

                    </div>

                  
<?php
                 echo do_shortcode('[contact-form-7 id="312" title="Contact form 1"]')  ;
 
            ?>           

                    
                </div>

            </div>

        </section>



<?php get_footer();?>