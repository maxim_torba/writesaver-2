
<?php

/*
 * Template Name: Customer Dashboard Open Past
 */

get_header();
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "customer") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}
?>

<section>

    <div class="breadcum">

        <div class="container">

            <div class="page_title">

                <h1>Dashboad</h1>

            </div>

        </div>

    </div>

</section>

<section class="uploaded_file_main">

    <div class="clickable">

        <a href="javascript:void(0);" class="openre">View all submitted documents <i class="fa fa-angle-down" aria-hidden="true"></i></a>                

    </div>

    <div class="collapsible_content" style="display: none;">

        <div class="inner_content submitted_docs">

            <div class="container-fluid">

                <div class="collapsible_slider">

                    <div class="create_new_doc">

                        <button type="button">Create new</button>

                    </div>

                    <div class="dashboard_content_slider">

                        <h5>All submitted documents</h5>

                        <div class="dashboard_collapsible_slider">

                            <ul class="slides">

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name10</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>In process</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name09</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>In process</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name08</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>Completed</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name07</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>Completed</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name06</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>Completed</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name05</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>Completed</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name04</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>Completed</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name03</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>Completed</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name02</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>Completed</span></p>

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="full_content">

                                        <div class="full_content_header">

                                            <h5>Doc name01</h5>

                                        </div>

                                        <div class="status">

                                            <p>Status : <span>Completed</span></p>

                                        </div>

                                    </div>

                                </li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

            <div class="clickable">

                <a href="javascript:void(0);" class="closer">Close <i class="fa fa-angle-up" aria-hidden="true"></i></a>                

            </div>

        </div>                

    </div>

</section>

<section>

    <div class="container">

        <div class="privacy customer">

            <div class="row service">

                <div class="col-sm-5">

                    <div class="total_ammount credit">

                        <div class="left">

                            <h4>1000<span>Words</span></h4>

                            <p>Remaining credit<a href="#">Upgrade Plan</a></p>                                    

                        </div>

                        <div class="right"></div>

                    </div>

                </div>

                <div class="col-sm-5">

                    <div class="total_ammount submitted">

                        <div class="left">

                            <h4>2000<span>Docs</span></h4>

                            <p>Total submitted<a href="#">View all</a></p>                                    

                        </div>

                        <div class="right"></div>

                    </div>

                </div>

                <div class="col-sm-2">

                    <label class="cloud_file">

                        Upload Your Doc.

                        <input type="file" />

                    </label>

                </div>

            </div>

            <div class="doc_name">

                <h2>Doc name10</h2>

            </div>

            <div class="main_editor">

                <div class="text_editor">

                    <div class="used_word">

                        <span>Word Count:</span><span class="count">240</span>

                    </div>

                    <textarea>

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas efficitur et dolor et rhoncus. Integer accumsan ultrices est, maximus mattis leo pellentesque sed. Phasellus lacus nibh, finibus nec imperdiet at, molestie eu ante. Maecenas eget purus egestas, pulvinar ante a, interdum libero. Nulla interdum velit a tellus eleifend tempus. Praesent laoreet viverra orci ac maximu. Aenean faucibus et felis at gravida. Fusce vel eleifend dui. Nunc posuere commodo diam, eget dictum enim tempor non. Intege eu turpis laoreet, placerat ipsum semper, maximus sem. Praesent elementum ultricies tincidunt. Class aptent taciti socios ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus aliquet, quam eget facilisis sodales, enim metus tinciunt purus, ac bibendum nunc elit a magna. Duis ac sapien sed ipsum sodales scelerisque in eu purus.



                                Interdum et malesuada fames ac ante ipsum primis in faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In nec risus leo. Nam scelerisque quam elit, vel efficitur eros egestas convallis. Integer sagittis magna sit amet nulla accumsan bibendum. Proin est augue, hendrerit ut lobortis eget, luctus a leo. Nunc nec mollis nisl. Cura varius sem ipsum, vel porttitor nisi tincidunt et. Pellentesque cursus et ipsum laoreet consectetur. Sed eget arcu et neque fringil gravida. Vivamus pulvinar, mauris a mattis eleifend, tellus libero pretium ligula, consectetur accumsan ante leo eget lectus. Proin vulputate odio elit, et fermentum mi mattis ut. Vivamus cursus mauris non faucibus commodo. In neque arcu, vehicula.

                    </textarea>

                </div>

                <div class="submit_area">

                    <p>Your submission has been proofread by our free automated software. To have our proofreading team read your paper, just click below!</p>

                    <div class="btn_blue">

                        <a href="#" class="btn_sky pop_btn">Have this paper proofread!</a>

                    </div>                            

                </div>

            </div>

        </div>

    </div>

</section>
<?php get_footer(); ?>